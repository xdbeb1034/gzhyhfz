# 微信公众号已关注用户能分组吗？怎么分？

实现微信公众号已关注用户分组，微号帮提供了粉丝分组批量转移功能实现，可以实现公众号在线给用户分组，将用户分组条件设置好通过功能提交给微号帮，微号帮自动进入后台处理任务，批量给公众号用户分组，与公众号后台用户管理功能同步，用户分组管理有利于公众号开展个性化服务，对不同分组的用户营销不同的内容，提高公众号转化率。

[微信公众号用户批量分组打标签功能](https://www.weihaobang.com/jiaocheng?fm=fang&id=2208)，支持公众号给用户在线分组，可以根据公众号用户的关注时间、地区、性别、openid、数量的条件批量分组，通过功能设置用户分组条件，微号帮自动进入后台任务系统管理，其过程无需工作人员看守，微号帮自动给符合条件的用户分组;通过功能可以给公众号实现一万个粉丝分组、划分北京地区的用户、用户分组之间转移、微商城用户openid导出分组等场景。

1、微号帮：微信公众号用户批量分组功能

![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/154139_1c4f1523_9447184.jpeg "微号帮_粉丝分组批量转移-1.jpg")

2、设置公众号用户批量分组条件

![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/154146_57df6c62_9447184.jpeg "微号帮_粉丝分组批量转移-2.jpg")

3、微号帮：公众号用户批量分组自动任务列表

![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/154155_e9d5484d_9447184.jpeg "微号帮_粉丝分组批量转移-3.jpg")

4、功能自动同步公众号后台用户管理

![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/154203_5a556052_9447184.jpeg "自动与公众号用户管理同步.jpg")

微号帮，微信公众号第三方功能助手平台，现所有功能注册登录授权添加公众号，免费试用7天!


[证件照](https://www.koutubang.com/?fm=fang)
